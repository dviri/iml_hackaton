import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import tensorflow as tf
import numpy as np
from sklearn import svm

# Importing the dataset
import load_headlines

options = {"min_df":5, "max_df":0.7, "ngram_range":(1, 6), "analyzer":'char_wb', "lowercase":False,
                                          "strip_accents":'unicode'}

class Classifier(object):
    classifier = None

    def __init__(self):
        dataset_ = pd.read_csv('haaretz.csv')
        # X = dataset.iloc[:, :].values
        # y = np.ones(X.shape)
        # X = np.reshape(X, [len(X)])
        X, y = load_headlines.load_dataset()
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=0.05, random_state=0)
        self.classifier = self.train(self.X_train, self.y_train)

    def classify(self):
        """
        Recieves a list of m unclassified tweets, and predicts for each one which celebrity posted it.
        :param X: A list of length m containing the tweet's texts (strings)
        :return: y_hat - a vector of length m that contains integers between 0 - 9
        """
        X_test = self.vectorizer.transform(self.X_test)
        return self.classifier.predict(X_test)


    def train(self, training_instances, training_labels):
        # self.vectorizer = TfidfVectorizer(min_df=5, max_df=0.7, ngram_range=(1, 6), analyzer='char_wb', lowercase=False,
        #                                   strip_accents='unicode')
        self.vectorizer = TfidfVectorizer(**options)
        X = self.vectorizer.fit_transform(training_instances)
        classifier = svm.LinearSVC()
        # classifier.fit(X, training_labels)
        classifier.fit_transform(X, training_labels)
        return classifier


classifier = Classifier()
res = classifier.classify()

size = len(res)
score = 0.0
for i, val in enumerate(res):
    if classifier.y_test[i] == res[i]:
        score += (1.0 / float(size))
        # score += (()/size)

print(score)
